#
# This registry is used for in-memory lists of things that need to be displayed
# or queried to users. The registry is agnostic about cimtypes, but it does
# does need the objects in the storage to have a name and a metadata attribute.
#

import unittest, uuid, copy

class Registry(object):
    """
    Provides the registry class for lists of objects which need to be
    query-able and display-able.
    """

    def __init__(self, content_type):
        """
        Initialise registry with
        :param content_type: the type of objects stored in the registry
        This is only a hint, content type is not enforced.
        """
        self.content_type = content_type
        self.names = []
        self.content = {}

    def __len__(self):
        return len(self.content)

    def __iter__(self):
        return iter(self.content.keys())

    def add(self, obj, replace=False):
        """
        Add an object to the registry
        :param obj: A CIM document instance
        :param replace: Allow replacements
        :return: 1 if successful
        """
        assert hasattr(obj, 'meta')
        assert hasattr(obj, 'uid')
        assert hasattr(obj, 'name')

        # want to ensure the registry object is not the actual object
        myobj = copy.deepcopy(obj)
        uid, name = myobj.uid, myobj.name
        if uid in self.content:
            if replace:
                self.delete(uid)
            else:
                raise ValueError('Object %s(%s) already in registry' % (name, uid))
        self.content[uid] = myobj
        self.names.append((name, uid))
        # FIXME: At scale this will need a bisect algorithm on insertion ...
        self.names.sort()
        return 1

    def get(self, uid):
        """
        Return specific object
        :param uid: id of object of interest
        :return: object
        """

        # This copy is needed to avoid subsequent changes to extracted
        # items affecting the registry content (see changes unit test)
        return copy.deepcopy(self.content[uid])

    def delete(self, uid):
        """
        Remove item from registry
        :param uid: identifier of item to remove
        :return: None
        """
        if uid not in self.content:
            raise ValueError("Can't delete non existent doc %s " % uid)
        n = self.content[uid].name
        index = self.names.index((n, uid))
        del self.content[uid]
        del self.names[index]

    def find(self, string):
        '''
        Find objects with string in their name
        :param string: string
        :return: set of objects containing the string in their name
        '''
        return [self.content[u] for (n, u) in self.names if string in n]

    def __add__(self, other):
        """ Add two registries together
        :param other: Another registry
        :return: concatenated registries
        """
        assert isinstance(other,Registry)
        x = copy.deepcopy(self)
        x.content_type += '+%s' % other.content_type
        for d in other:
            x.add(other.get(d))
        return x


class TestRegistry(unittest.TestCase):
    """ Simple unittests for registry
    """

    class Mock(object):

        class MockMeta(object):
            def __init__(self):
                self.uid = str(uuid.uuid1())

        def __init__(self, cimtype, name):
            self.name = name
            self.meta = self.MockMeta()
            self.cimType = cimtype

        def __eq__(self,other):
            for f in ['name','cimType']:
                if getattr(self,f)<>getattr(other,f): return 0
            if self.uid <> other.uid: return 0
            return 1

        def __str__(self):
            return self.name

    def testAdd(self):
        ctype = 'ab'
        r = Registry(ctype)
        o = self.Mock(ctype, 'Fred')
        r.add(o)
        self.assertEqual(len(r),1)

    def testGet(self):
        ctype = 'ab'
        r = Registry(ctype)
        o = self.Mock(ctype, 'Fred')
        r.add(o)
        y=r.get(o.uid)
        self.assertEqual(y,o)

    def testDel(self):
        ctype = 'ab'
        r = Registry(ctype)
        o = self.Mock(ctype, 'Fred')
        r.add(o)
        r.delete(o.uid)
        self.assertEqual(len(r),0)
        self.assertEqual(r.content,{})
        self.assertEqual(r.names,[])

    def testFind(self):
        ctype = 'ab'
        r = Registry(ctype)
        o = self.Mock(ctype, 'Fred')
        o1 = self.Mock(ctype, 'Fred Dagg')
        o2 = self.Mock(ctype, 'Wayne')
        for i in [o, o1, o2]: r.add(i)
        self.assertEqual(r.find('Fred'),[o,o1])
        self.assertEqual(r.find('Wayne'),[o2,])

    def testIn(self):
        ctype = 'ab'
        r = Registry(ctype)
        o = self.Mock(ctype, 'Fred')
        o1 = self.Mock(ctype, 'Fred Dagg')
        o2 = self.Mock(ctype, 'Wayne')
        for i in [o, o1, o2]: r.add(i)
        uid=o1.uid
        self.assertTrue(uid in r)

    def testReplace(self):
        ctype = 'ab'
        r = Registry(ctype)
        o = self.Mock(ctype, 'Fred')
        r.add(o)
        o.name = 'Dagg'
        r.add(o,True)

    def testChanges(self):
        ctype = 'ab'
        r = Registry(ctype)
        o = self.Mock(ctype, 'Fred')
        r.add(o)
        p = r.get(o.uid)
        p.name = 'Dagg'
        m = r.get(o.uid)
        self.assertEqual(o,m)

    def testAddRegistries(self):
        r = Registry('ab')
        s = Registry('cd')
        o = self.Mock('ab','Fred')
        r.add(o)
        o2 = self.Mock('cd','Sue')
        s.add(o2)
        t = r+s
        self.assertEqual(t.content_type,'ab+cd')
        self.assertEqual(len(t),2)

    def testAddRegistries(self):
        r = Registry('ab')
        s = Registry('cd')
        o = self.Mock('ab','Fred')
        r.add(o)
        o2 = self.Mock('cd','Sue')
        s.add(o2)
        r+=s
        self.assertEqual(r.content_type,'ab+cd')
        self.assertEqual(len(r),2)

if __name__ == "__main__":
    unitest.main()
