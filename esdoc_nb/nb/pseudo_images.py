#
# Holds code for plotting images inside gtk windows
#
import gtk
import os
import pseudo_utils as pu

class imageDialog(gtk.Dialog):
    """ Base class mixin for plotting images using anything that
    supports the image plotting api described herein.
    
    Provides 
        - an HBox for putting the image in - self.imageBox
        - a VBox for putting an image caption into self.caption
        - a button panel - self.panel which can be used
          for stock buttons (we provide quit by default).
        - an inner panel for image specific buttons (self.inner)
        
    If subclasses create disposable real files, then they must make sure
    the real file names are listed in self.internal_files
    """
    
    def __init__(self,title,padding=2):
        """ Initialise with a dialog title """
        super(imageDialog,self).__init__(title)
        super(imageDialog,self).set_modal(False)
        
        self.padding=padding
        self.internal_files=[]
        
        self.imageBox=gtk.HBox()
        self.vbox.pack_start(self.imageBox,padding=self.padding)
        
        self.caption=gtk.VBox()
        self.vbox.pack_start(self.caption,padding=self.padding)
        
        # Note the bug in action area implementation means it wont
        # respect homogeneity setting, so we have to create our own.
        
        frame=gtk.Frame()
        self.inner=gtk.HBox()
        frame.add(self.inner)
        self.panel=gtk.HBox()
        
        self.widgets=[self.imageBox,self.caption,frame,self.inner,self.panel]
        
        self.vbox.pack_end(self.panel,expand=False,padding=self.padding)
        self.panel.pack_start(frame,expand=False,padding=self.padding)
        
        b=pu.ibutton(gtk.STOCK_QUIT,'Quit')
        self.panel.pack_end(b,expand=False,fill=False,padding=self.padding)
        b.connect('clicked',self.quit,None)
        self.widgets.append(b)
        
    def quit(self,widget,data):
        """ Kill dialog and remove internal files """
        for f in self.internal_files:
            os.remove(f)
        self.destroy()
        
    def show(self):
        for w in self.widgets: w.show()
        super(imageDialog,self).show()
    


