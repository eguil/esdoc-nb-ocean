__author__ = 'BNL28'
import sys
from inspect import getmembers, ismodule

def get_items(base='cmip6'):

    """ Parse the schema directory to return a set of sub-modules """

    results = {}

    base_module = __import__(base)

    entities = base_module.__all__
    qualified_entities = ['%s.%s' % (base, e) for e in entities]
    map(__import__, qualified_entities)
    for s in sys.modules:
        if s.startswith(base+'.'):
            m = sys.modules[s]
            contents = getmembers(m)
            results[m.__name__] = [o for o in contents if not o[0].startswith('__')]

    return results

if __name__ == "__main__":
    # you might run this to make sure all your extension modules import!
    r = get_items()
    for i in r:
        print i, r[i]