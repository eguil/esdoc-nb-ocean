# The ID variable must match the automatically generated id.
ID = None

# The TYPE variable must contain the CIM type being specialised in
# this file.
TYPE = 'science.process'

# The CIM variable must contain the CIM version being specialised.
CIM = '2.0'

# The CONTACT, AUTHORS and DATE variables must be set.
CONTACT = None

AUTHORS = None

DATE = None

# The VERSION variable must be set to an independent version number
# for this file.
VERSION = None

# ====================================================================
# PROPERTIES
# ====================================================================
PROPERTIES = {

    # ----------------------------------------------------------------
    # MANIFEST
    # ----------------------------------------------------------------
    'short_name': None,
    'description': None,

    'details': [],

    'sub_process': [],

    # ----------------------------------------------------------------
    # DETAILS
    # ----------------------------------------------------------------

    # ----------------------------------------------------------------
    # SUB-PROCESSES
    # ----------------------------------------------------------------

    # ----------------------------------------------------------------
    # SUB-PROCESS DETAILS
    # ----------------------------------------------------------------
}

# ====================================================================
# ENUMERATIONS
# ====================================================================
ENUMERATIONS = {}
