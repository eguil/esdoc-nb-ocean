ID = 'cmip6.ocean.grid'

TYPE = 'science.grid'

CIM = '2.0'

CONTACT = 'Eric Guilyardi (e-mail)'

AUTHORS = 'Eric Guilyardi'

DATE = '2016-04-19'

VERSION = '0.1'

# ====================================================================
# GRID: PROPERTIES
# ====================================================================
DESCRIPTION = 'Ocean grid and discretisation'

# ====================================================================
# PROPERTIES
# ====================================================================
DETAILS = {

    # ----------------------------------------------------------------
    # DETAILS
    # ----------------------------------------------------------------
    'vertical_grid': {
         'description': 'Properties of vertical coordinate in ocean',
         'short_name': 'Properties of vertical coordinate in ocean',
         'number_of_levels': (
             'int', '1.1',
             'Number of vertical levels'),
    },

    'horizontal_grid': {
         'description': 'Properties of H coordinate in ocean',
         'short_name': 'Properties of H coordinate in ocean',
         'horizontal_grid_type': (
             'ENUM:horiz_grid_types', '1,1',
             'Horizontal grid type'),
         'number_of_xy_gridpoints':(
             'int', '0.1',
             'Total number of horizontal points on computational grid'),
    },

    # ----------------------------------------------------------------
    # DISCRETISATION
    # ----------------------------------------------------------------
    'discretisation': {
        'short_name': 'Type of discretisation scheme in ocean',
        'description': 'Type of discretisation scheme in ocean',
        'details': [('horizontal_discretisation',
                     'vertical_discretisation'),
                ],
    },

    # ----------------------------------------------------------------
    # DISCRETISATION DETAILS
    # ----------------------------------------------------------------
    'vertical_discretisation': {
         'description': 'Properties of vertical coordinate in ocean',
         'short_name': 'Properties of vertical coordinate in ocean',
         'vertical_coord_type': (
             'ENUM:vertical_coord_types', '1.1',
             'Type of vertical coordinates in ocean'),
         'partial_steps': (
             'bool', '1.1',
             'Using partial steps with Z or Z* vertical coordinate in ocean ?'),
    },
    'horizontal_discretisation': {
        'short_name': 'Type of horizontal discretisation scheme in ocean',
        'description': 'Type of horizontal discretisation scheme in ocean',
        'horiz_discret_scheme': (
            'ENUM:horiz_scheme_types', '1.1',
            'Horizontal discretisation scheme in ocean'),
        'ocean_pole_singularity_treatment': (
            'str', '1.1',
            'Describe how the North Pole singularity is treated (filter, pole rotation/displacement, artificial island, ...)'),
    },

}

# ====================================================================
# ENUMERATIONS
# ====================================================================
ENUMS = {

    'horiz_grid_types': {
        'short_name': 'Types of horizonal grid in ocean',
        'description': 'Types of horizonal grid in ocean',
        'members': [
            ('Latlon','tbd'),
            ('Rotated north pole','tbd'),
            ('Two north poles (ORCA-style)','tbd'),
            ('Other','tbd'),
        ],
    },

    'horiz_scheme_types': {
        'short_name': 'Types of horizonal scheme in ocean',
        'description': 'Types of horizonal scheme in ocean',
        'members': [
            ('Finite difference / Arakawa B-grid','tbd'),
            ('Finite difference / Arakawa C-grid','tbd'),
            ('Finite difference / Arakawa E-grid','tbd'),
            ('Finite volumes', 'tbd'),
            ('Finite elements', 'tbd'),
            ('Other', 'tbd')
        ],
    },

    'vertical_coord_types': {
        'short_name': 'Types of vertical coordinates in ocean',
        'description': 'Types of vertical coordinates in ocean',
        'members': [
            ('Z-coordinate','tbd'),
            ('Z*-coordinate', 'tbd'),
            ('S-coordinate', 'tbd'),
            ('Isopycnic - sigma 0','Density referenced to the surface'),
            ('Isopycnic - sigma 2','Density referenced to 2000 m'),
            ('Isopycnic - sigma 4','Density referenced to 4000 m'),
            ('Isopycnic - other','Other density-based coordinate'),
            ('Hybrid / Z+S', 'tbd'),
            ('Hybrid / Z+isopycnic', 'tbd'),
            ('Hybrid / ALE', 'tbd'),
            ('Hybrid / other', 'tbd'),
            ('Pressure referenced (P)', 'tbd'),
            ('P*','tbd'),
            ('Z**', 'tbd'),
            ('Other', 'tbd')
        ],
    },

}