ID = 'cmip6.ocean.key_properties'

CONTACT = 'Eric Guilyardi'

AUTHORS = None

DATE = '2016-04-19'

VERSION = '0.1'

TYPE = 'cim.2.science.key_properties'

# ====================================================================
# KEY PROPERTIES: PROPERTIES
# ====================================================================
DESCRIPTION = 'Key properties of the ocean'

# ====================================================================
# KEY PROPERTIES: DETAILS
# ====================================================================
DETAILS = {
    'general': {
        'short_name': '',
        'description': '',
        'model_family': (
            'ENUM:model_family_type', '1.1',
            'Type of ocean model.'),
        'basic_approximations': (
            'ENUM:basic_approximations_attributes', '1.N',
            'Basic approximations made in the ocean.',),
        'prognostic_variables': (
            'ENUM:prognostic_vars_types', '1.N',
            'List of prognostic variables in the ocean component.'),
    },
    'seawater_properties': {
        'description': 'Physical properties of seawater in ocean',
        'short_name': 'Properties of seawater in ocean',
        'seawater_eos_type': (
            'ENUM:seawater_eos_types', '1.1',
            'Type of EOS for sea water'),
        'ocean_freezing_point': (
            'str', '1.1',
            'Describe freezing point in ocean (fixed or varying)'),
        'ocean_specific_heat': (
            'str', '1.1',
            'Describe specific heat in ocean (fixed or varying)'),
    },
    'bathymetry': {
        'description':'Properties of bathymetry in ocean',
        'short_name': 'Properties of bathymetry in ocean',
        'bathymetry_reference_dates': (
            'ENUM:ocean_bathymetry_ref_dates', '1.1'
            'Reference date of bathymetry'),
        'ocean_bathymetry_type': (
            'bool', '1.1',
            'Is the bathymetry fixed in time in the ocean ?'),
        'ocean_smoothing': (
            'str', '1.1',
            'Describe any smoothing or hand editing of bathymetry in ocean'),
        'ocean_bathymetry_source': (
            'str', '1.1',
            'Describe source of bathymetry in ocean'),
    },
    'nonoceanic_waters': {
        'short_name': 'Treatment of non-oceanic waters (isolated seas and river mouth)',
        'description': 'Describe if/how isolated seas and river mouth mixing or other specific treatment is performed',
        'ocean_nonoceanic_waters': (
            'str', '0.1',
            'Describe if/how isolated seas and river mouth mixing or other specific treatment is performed'),
    },
}
# ====================================================================
# KEY PROPERTIES: EXTENT
# ====================================================================
EXTENT = {}

# ====================================================================
# KEY PROPERTIES: RESOLUTION
# ====================================================================
RESOLUTION = {
    'resolution': {
        'short_name': 'NAME',
        'description': 'DESCRIPTION',
        'thickness_of_surface_level': (
            'float', '0.1',
            'Thickness in metres of surface ocean level (e.g. 1)'),
    },
}
# ====================================================================
# KEY PROPERTIES: TUNING
# ====================================================================
TUNING = {}

# ====================================================================
# KEY PROPERTIES: CONSERVATION
# ====================================================================
CONSERVATION = {
    'conservation_properties': {
        'short_name': 'Ocean conservation properties',
        'description': 'Properties conserved in the ocean component',
        'conservation_scheme': (
            'ENUM:conservation_props_types', '1.N'
            'Conservation scheme in ocean'),
        'ocean_conservation_method': (
            'char', '1.1',
            'Describe how conservation properties are ensured in ocean'),
    },
}

# ====================================================================
# ENUMERATIONS
# ====================================================================
ENUMERATIONS = {
    'ocean_basic_approx_types': {
        'short_name': 'Types of basic approximation in ocean',
        'description': 'Types of basic approximation in ocean',
        'members': [
            ('Primitive equations','tbd'),
            ('Non-hydrostatic', 'tbd'),
            ('Boussinesq', 'tbd'),
            ('Other', 'tbd')
        ],
    },

    'prognostic_vars_types': {
        'short_name': 'Types of basic approximation in ocean',
        'description': 'Types of basic approximation in ocean',
        'members': [
            ('Potential temperature','tbd'),
            ('Conservative temperature','tbd'),
            ('Salinity','tbd'),
            ('U-velocity','tbd'),
            ('V-velocity','tbd'),
            ('W-velocity','tbd'),
            ('SSH','Sea Surface Height'),
            ('Other', 'Other prognostic variables')
        ],
    },

    'seawater_eos_types': {
        'short_name': 'Types of seawater Equation of State in ocean',
        'description': 'Types of seawater Equation of State in ocean',
        'members': [
            ('Linear','tbd'),
            ('Mc Dougall et al.', 'tbd'),
            ('Jackett et al. 2006', 'tbd'),
            ('TEOS 2010', 'tbd'),
            ('Other', 'tbd')
        ],
    },

    'bathymetry_ref_dates': {
        'short_name': 'List of reference dates for bathymetry in ocean',
        'description': 'List of reference dates for bathymetry in ocean',
        'members': [
            ('Present day','tbd'),
            ('21000 years BP', 'tbd'),
            ('6000 years BP', 'tbd'),
            ('LGM', 'Last Glacial Maximum'),
            ('Pliocene', 'tbd'),
            ('Other', 'tbd')
        ],
    },

    'conservation_props_types': {
        'short_name': 'Types of conservation properties in ocean',
        'description': 'Types of conservation properties in ocean',
        'members': [
            ('Energy','tbd'),
            ('Enstrophy','tbd'),
            ('Salt', 'tbd'),
            ('Volume of ocean', 'tbd'),
            ('Other', 'tbd')
        ]
    },
}

