__author__ = 'BNL28'
version = '0.0.4'

#
# PROCESS ===============================================================================
#

si_properties = {
    'base': 'science.process',
    'values': {
        'name': 'Sea Ice Processes',
        'id': 'cmip6.si.prop',
        'context': 'Collection of processes which control sea ice.',
        'details': ['si_base_properties'],
        'sub-processes': ['si_thermodynamics',
                          'si_dynamics',
                          'si_radiative_processes'],
        'algorithms': ['si_thermodynamics_budget']
        }
}

#
# SUB-PROCESSES ===========================================================================
#

si_thermodynamics = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Sea Ice Thermodynamics',
        'id': 'cmip6.si.prop.thermo',
        'context': 'Characteristics of sea ice thermodynamics processes',
        'details': ['si_thermo_processes',
                    'si_snow_processes',
                    'si_vertical_heat_diffusion',
                   ]
    }
}

si_dynamics = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Sea Ice Dynamics',
        'id': 'cmip6.si.prop.dyn',
        'context': 'Characteristics of the Sea Ice Dynamics',
        'details': ['si_horizontal_advection',
                   'si_transport_in_thickness_space',
                   'si_redistribution',
                   'si_rheology']
    }
}

si_radiative_processes = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Sea Ice Radiative Processes',
        'id': 'cmip6.si.prop.radproc',
        'context': 'Collected properties of radiation in sea ice thermodynamics',
        'details': ['si_radiative_process_methods',]
    }
}

#
# ALGORITHMS ===========================================================================
#

si_thermodynamics_budget = {
    'base': 'science.algorithm',
    'values': {
        'name': 'Thermodynamics Budget',
        'id': 'cmip6.si.prop.therm_budget',
        'context': 'Information required to close the thermodynamics budget',
    }
}

#
# DETAILS ===========================================================================
#
si_base_properties = {
    'base': 'science.detail',
    'values': {
        'context': 'Methods used to represent sea ice',
        'id': 'cmip6.si.prop.layer',
        'name': 'Sea Ice Properties',
        'select': 'layering',
        'from_vocab': 'cmip6.si.prop.layer.types.%s' % version,
        'with_cardinality': '1.1',
    },
    'properties':[
    ('new_ice_formation', 'text', '0.1',
            'Description of method used to form new ice'),
    ('ice_lateral_melting', 'text', '0.1',
            'Method by whichg sea ice lateral melting occurs'),
    ('ice_surface_sublimation', 'text', '0.1',
            'Method by which water sublimes on ice surface'),
    ('water_ponds','text','0.1',
            'Brief description of water ponding on ice')
    ]
}
si_layering_types = {
    'name': 'Sea Ice Layering Types',
    'id': 'cmip6.si.prop.layer.types',
    'members': [
            ('2-levels', 'Simulation uses two layers.'),
            ('Multi-level', 'Simulation uses more than two layers'),
            ('Ice-Types', 'Simulation does not use layers, but has multiple ice types per grid cell'),
        ]
    }

si_snow_processes = {
    'base': 'science.detail',
    'values': {
        'name': 'Sea Ice Snow Processes',
        'id': 'cmip6.si.prop.thermo.snow',
        'context': 'Snow processes in sea ice thermodynamics',
        'select': 'process_type',
        'from_vocab': 'cmip6.si.prop.thermo.snow.types.%s' % version,
        'with_cardinality': '1.N'
    }
}

si_snow_process_types = {
    'name': 'Types of snow processes',
    'id': 'cmip6.si.prop.thermo.snow.types',
    'members': [
        ('single-layered heat diffusion', None),
        ('multi-layered heat diffusion', None),
        ('snow aging scheme', None),
        ('snow ice scheme', None),
    ]
}

si_radiative_process_methods = {
    'base': 'science.detail',
    'values': {
        'name':'Sea Ice Radiative Process Details',
        'id':'cmip6.si.prop.radproc.det',
        'context':'Additional information about radiative processes in sea ice.',
    },
    'properties': [
        ('surface_albedo', 'text', '0.1',
            'Method used to handle surface albedo'),
        ('ice_radiation_transmission','text','0.1',
            'Method by which solar radiation through ice is handled'),
        ]
    }
si_vertical_heat_diffusion = {
    'base': 'science.detail',
    'values': {
        'name': 'Vertical Heat Diffusion',
        'id': 'cmip6.si.prop.thermo.vhd',
        'context': 'Characteristics of vertical heat diffusion in sea ice.',
    },
    'properties':[
        ('num_of_layers', 'int', '1.1',
            'Number of layers used for vertical heat diffusion'),
        ('regular_grid', 'bool', '0.1',
            'If multiple layers, are they regularly distributed?'),
        ('based_on_semtner', 'bool','1.1',
            'Is method based on semtner 1976?')
    ]
}

si_thermo_processes = {
    'base': 'science.detail',
    'values': {
        'name':'Thermodynamic Process Details',
        'context': 'Information about basal heat flux and brine inclusions',
        'id':'cmip6.si.prop.thermo.brbhf',
        'select': 'brine_inclusion_method',
        'from': 'si_thermo_brine_types',
        'with_cardinality': '0.1',
    },
    'properties': [
        ('basal_heat_flux', 'text', '0.1', 'Method by which basal heat flux is handled'),
        ('brine_inclusions', 'text', '0.1', 'Method by which brine inclusions are handled')
    ]
}

si_thermo_brine_types = {
    'name': 'Brine Inclusion Methodology',
    'id': 'cmip6.si.thermo.brine.types',
    'members': [
        ('None', 'No brine inclusions included in sea ice thermodynamics'),
        ('Heat Reservoir', 'Brine inclusions treated as a heat reservoir'),
        ('Thermal Fixed Salinity', 'Thermal properties depend on S-T (with fixed salinity)'),
        ('Thermal Varying Salinity', 'Thermal properties depend on S-T (with varying salinity'),
    ]
}

si_horizontal_advection = {
    'base': 'science.detail',
    'values': {
        'name': 'Horizontal Advection of Sea Ice',
        'id': 'cmip6.si.prop.dyn.hadv',
        'context': 'Method of horizontal advection',
        'select': 'method',
        'from': 'si_transport_methods',
        'with_cardinality': '0.1',
    }
}

si_transport_in_thickness_space = {
    'base':'science.detail',
    'values': {
        'name':'Sea Ice vertical transport',
        'id':'cmip6.si.prop.dyn.vtrans',
        'context': 'Method of ice migration in thickness',
        'select': 'method',
        'from': 'si_transport_methods',
        'with_cardinality':'0.1',
    }

}

si_transport_methods = {
    'name': 'Transport Methods',
    'id': 'cmip6.si.trans.methods',
    'members': [
        ('Incremental Re-mapping','(including Semi-Lagrangian)'),
        ('Prather', None),
        ('Eulerian', None)
    ]
}

si_redistribution = {
    'base': 'science.detail',
    'values': {
        'name':'Sea Ice Redistribution',
        'id':'cmip6.si.prop.dyn.redis',
        'context':'Additional processes which can redistribute sea ice.',
        'select':'processes',
        'from':'si_redistribution_types',
        'with_cardinality':'0.N',
    },
    'properties':[
        ('ice_strength_formulation','text','0.1','Describe how ice-strength is formulated'),
    ]
}

si_redistribution_types = {
    'name':'Sea Ice Redistribution Types',
    'id':'cmip6.si.enum.redis',
    'members': [
        ('Rafting', None),
        ('Ridging', None),
    ]
}

si_rheology = {
    'base': 'science.detail',
    'values': {
        'context': 'Process by ',
        'name': 'Sea Ice Rheology',
        'id': 'cmip6.si.prop.dyn.rheo',
        'select': 'methods of ice deformation',
        'from': 'si_rheology_types',
        'with_cardinality': '1.1'
    }
}

si_rheology_types = {
        'name': 'Sea Ice rheology types',
        'id': 'cmip6.si.prop.dyn.rheo.types',
        'members': [
            ('free-drift', None),
            ('Mohr-Coloumb', None),
            ('visco-plastic', None),
            ('elastic-visco-plastic','EVP'),
            ('granular', None),
            ('other', None)
        ]
    }



