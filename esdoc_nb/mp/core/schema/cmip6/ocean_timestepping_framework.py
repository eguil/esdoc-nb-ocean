ID = 'cmip6.ocean.timestepping_framework'

TYPE = 'science.process'

CIM = '2.0'

CONTACT = 'Eric Guilyardi'

AUTHORS = ''

DATE = '2016-04-19'

VERSION = '0.1'

# ====================================================================
# PROPERTIES
# ====================================================================
PROPERTIES = {

    # ----------------------------------------------------------------
    # MANIFEST
    # ----------------------------------------------------------------
    'short_name': 'Ocean time stepping framework',
    'description': 'Characteristics of ocean time stepping framework',

    'details': ['timestepping_attributes',
                'timestepping_tracers',
                'barotropic_solver',
                'barotropic_momemtum'
            ],

    'sub_process': [],

    # ----------------------------------------------------------------
    # DETAILS
    # ----------------------------------------------------------------
    'timestepping_attributes': {
        'description': 'Properties of time stepping in ocean',
        'short_name': 'Properties of time stepping in ocean',
        'time_step': (
            'int', '1.1',
            'Ocean time step in seconds'),
        'diurnal_cycle': (
            'ENUM:diurnal_cycle_types', '1.1',
            'Diurnal cycle type'),
            #'closed'),
    },
    'timestepping_tracers': {
        # Scientific context and properties
        'short_name': 'Timestepping for tracers',
        'description': 'Properties of timestepping for tracers in ocean',
        'timestepping_tracers_scheme':(
            'ENUM:ocean_timestepping_types','1.1',
            'Time stepping tracer scheme'),
        },

    'barotropic_solver': {
        'short_name': 'Barotropic solver in ocean',
        'description': 'Properties of barotropic solver in ocean',
        'barotropic_solver_scheme':(
            'ENUM:ocean_timestepping_types','1.1',
            'Barotropic solver scheme'),
        },

    'barotropic_momentum': {
        'short_name': 'Barotropic momentum in ocean',
        'description': 'Properties of barotropic momentum in ocean',
        'barotropic_momentum_scheme':(
            'ENUM:ocean_timestepping_types','1.1',
            'Barotropic momentum scheme'),
        },


}

# ====================================================================
# ENUMERATIONS
# ====================================================================
ENUMERATIONS = {

    'diurnal_cycle_types' : {
        'shortname': 'Types of diurnal cycle resolution in ocean',
        'description': 'Types of diurnal cycle resolution in ocean',
        'members': [
            ('None','No diurnal cycle in ocean'),
            ('Via coupling','Diurnal cycle via coupling frequency'),
            ('Specific treatment', 'Specific treament'),
            ]
        },
    
    'ocean_timestepping_types' : {
        'short_name': 'Type of timestepping scheme in ocean',
        'description': 'Type of timestepping scheme in ocean',
        'members': [
            ('Leap-frog + Asselin filter', 'Leap-frog scheme with Asselin filter'),
            ('Leap-frog + Periodic Euler backward solver', 'Leap-frog scheme with Periodic Euler backward solver'),
            ('Predictor-corrector', 'Predictor-corrector scheme'),
            ('AM3-LF (ROMS)', 'AM3-LF used in ROMS'),
            ('Forward-backward', 'Forward-backward scheme'),
            ]
        },
    }
