ID = 'cmip6.ocean'

TYPE = 'science.scientific_realm'

CIM = '2.0'

CONTACT = 'Eric Guilyardi'

AUTHORS = ''

DATE = '2016-04-10'

VERSION = '0.1'

# ====================================================================
# PROPERTIES
# ====================================================================
PROPERTIES = {

    # ----------------------------------------------------------------
    # MANIFEST
    # ----------------------------------------------------------------
    'short_name': None,
    'description': None,

    'name': 'ocean',
    'realm': 'ocean',

    'overview': '',

    'grid': ['ocean_grid'],

    'key_properties': ['ocean_key_properties'],

    'processes': [
        'ocean_timestepping_framework',
        'ocean_advection',
        'ocean_lateral_physics',
        'ocean_vertical_physics',
        'ocean_uplow_boundaries',
        'ocean_boundary_forcing',
    ]
}

# ====================================================================
# ENUMERATIONS
# ====================================================================
ENUMERATIONS = {}

